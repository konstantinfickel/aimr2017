#include "libcoordinates.h"

// Calculates the manhattan distance between two points
double calculateManhattanDistance(Cell* goal, Cell* current) { return fabs(current->i - goal->i) + fabs(current->j - goal->j); }

// Functions to convert positions among different data structures
PostureDouble cellToPosture(Cell cell) {
  PostureDouble postureCell;
  postureCell.x = ((double)cell.i) * GRID_SIZE;
  postureCell.y = ((double)cell.j) * GRID_SIZE;
  postureCell.theta = 0.0;
  postureCell.absoluteTheta = 0.0;
  return postureCell;
}

Cell postureToCell(PostureDouble posture) {
  Cell cell;
  cell.i = (int)round(posture.x / GRID_SIZE);
  cell.j = (int)round(posture.y / GRID_SIZE);
  return cell;
}

Coordinates postureToCoordinates(PostureDouble posture) {
  Coordinates coordinates;
  coordinates.i = (int)round(posture.x / GRID_SIZE);
  coordinates.j = (int)round(posture.y / GRID_SIZE);
  return coordinates;
}

PostureDouble coordinatesToPosture(Coordinates coordinates) {
  PostureDouble posture;
  posture.x = ((double)coordinates.i) * GRID_SIZE;
  posture.y = ((double)coordinates.j) * GRID_SIZE;
  posture.theta = 0.0;
  posture.absoluteTheta = 0.0;
  return posture;
}

Cell coordinatesToCell(Coordinates coordinates) {
  Cell result;
  result.i = coordinates.i;
  result.j = coordinates.j;
  return result;
}

// Check, if two cell objects are pointing towards the same coordinates
bool cellEquals(Cell* a, Cell* b) { return (a->i == b->i) && (a->j == b->j); }
