#ifndef LIBCOORDINATES_H_INCLUDED
#define LIBCOORDINATES_H_INCLUDED

#include "libposition.h"
#include "lists.h"
#include "maps.h"
#include "simulatorhack.h"

#define GRID_SIZE 50.0

double calculateManhattanDistance(Cell* goal, Cell* current);

PostureDouble cellToPosture(Cell cell);
Cell postureToCell(PostureDouble posture);

Coordinates postureToCoordinates(PostureDouble posture);
PostureDouble coordinatesToPosture(Coordinates coordinates);

Cell coordinatesToCell(Coordinates coordinates);

bool cellEquals(Cell* a, Cell* b);

#endif
