#include "libfuzzy.h"

// Calculate predicates using the position/ir-sensor-values
void calculateGoalPredicates(DirectionPredicatesDouble* predicates, PostureDouble* current, PostureDouble* goal) {
  double e_theta = calculateAngleDifference(current->theta, pmod(calculateAngle(current, goal), 2 * PI));
  double e_position = calculateDistance(current, goal);

  predicates->left = RampUp(e_theta, 0, 0.25 * PI);
  predicates->right = RampDown(e_theta, -0.25 * PI, 0);
  predicates->ahead = min(RampUp(e_theta, -0.6 * PI, 0), RampDown(e_theta, 0, 0.6 * PI));
  predicates->here = RampDown(e_position, 0, 60);
}

void calculateObstaclePredicates(DirectionPredicatesDouble* predicates, Sensors* ir) {
  predicates->right = RampUp(max(ir->sensor[5], ir->sensor[6]), NO_DANGER, FULL_DANGER);
  predicates->left = RampUp(max(ir->sensor[1], ir->sensor[2]), NO_DANGER, FULL_DANGER);
  predicates->ahead = RampUp(max(ir->sensor[0], ir->sensor[7]), NO_DANGER, FULL_DANGER);
  predicates->here =
      RampUp(max(max(max(ir->sensor[5], ir->sensor[6]), max(ir->sensor[0], ir->sensor[7])), max(ir->sensor[1], ir->sensor[2])), NO_DANGER,
             FULL_DANGER);
}

// Functions converting the [0,1]-value to speed and rotation values. Were found out by trial and error.
double ResponseToVel(double vel) { return (vel - (1.0 / 3.0)) * 1400; }

double ResponseToRot(double rot) { return -(rot - 0.5) * PI * 11; }

// Ruleset definitions
void gotoAvoidingObstaclesRuleset(GeneralPredicatesDouble* predicates) {
  DirectionPredicatesDouble* obstacle = predicates->obstacle;
  DirectionPredicatesDouble* position = predicates->position;

  // clang-format off
  IF (AND(position->left,  NOT(obstacle->left))); ROT(LEFT);
  IF (AND(position->left, AND(obstacle->left, NOT(obstacle->ahead)))); ROT(AHEAD);
  IF (AND(position->right, NOT(obstacle->right))); ROT(RIGHT);
  IF (AND(position->right, AND(obstacle->right, NOT(obstacle->ahead)))); ROT(AHEAD);
  IF (AND(position->ahead, NOT(obstacle->ahead))); ROT(AHEAD);
  IF (AND(position->ahead, AND(obstacle->ahead, NOT(obstacle->left)))); ROT(LEFT);
  IF (AND(position->ahead, AND(obstacle->ahead, NOT(obstacle->right)))); ROT(RIGHT);

  IF (obstacle->ahead); VEL(BACK);
  IF (OR(position->here, NOT(position->ahead))); VEL(NONE);
  IF (AND(position->ahead, AND(NOT(obstacle->ahead), OR(obstacle->left, obstacle->right)))); VEL(SLOW);
  IF (OR(AND(position->right, obstacle->right), AND(position->left, obstacle->left))); VEL(SLOW);
  IF (AND(position->ahead, NOT(OR(OR(obstacle->left, obstacle->right), obstacle->ahead)))); VEL(FAST);
  // clang-format on
}

void gotoRuleset(GeneralPredicatesDouble* predicates) {
  DirectionPredicatesDouble* position = predicates->position;

  // clang-format off
  IF (AND(position->left, NOT(position->here))); ROT(LEFT);
  IF (AND(position->right, NOT(position->here))); ROT(RIGHT);
  IF (OR(position->here, position->ahead)); ROT(AHEAD);
  IF (AND(position->ahead, NOT(position->here))); VEL(FAST);
  IF (OR(position->here, NOT(position->ahead))); VEL(NONE);
  // clang-format on
}

void avoidObstaclesRuleset(GeneralPredicatesDouble* predicates) {
  DirectionPredicatesDouble* obstacle = predicates->obstacle;

  // clang-format off
  IF (AND(obstacle->left,  NOT(obstacle->right))); ROT(RIGHT);
  IF (AND(obstacle->right, NOT(obstacle->left))); ROT(LEFT);
  IF (OR(NOT(obstacle->here), AND(obstacle->right, obstacle->left))); ROT(AHEAD);
  IF (obstacle->ahead); VEL(BACK);
  IF (AND(OR(obstacle->right, obstacle->left), NOT(obstacle->ahead))); VEL(SLOW);
  IF (NOT(OR(OR(obstacle->right,obstacle->left), obstacle->ahead))); VEL(FAST);
  // clang-format on
}

void improvedGotoRuleset(GeneralPredicatesDouble* predicates) {
  DirectionPredicatesDouble* position = predicates->position;

  // clang-format off
  IF (AND(position->left, NOT(position->here))); ROT(LEFT);
  IF (AND(position->right, NOT(position->here))); ROT(RIGHT);
  IF (OR(position->here, position->ahead)); ROT(AHEAD);
  IF (AND(position->ahead, NOT(position->here))); VEL(FAST);
  IF (OR(position->here, NOT(position->ahead))); VEL(NONE);
  // clang-format on
}

void improvedAvoidObstaclesRuleset(GeneralPredicatesDouble* predicates) {
  DirectionPredicatesDouble* obstacle = predicates->obstacle;

  // clang-format off
  IF (AND(obstacle->left,  NOT(obstacle->right))); ROT(RIGHT);
  IF (OR(AND(obstacle->right, NOT(obstacle->left)), obstacle->ahead)); ROT(LEFT);
  IF (OR(NOT(obstacle->here), AND(obstacle->right, obstacle->left))); ROT(AHEAD);
  IF (obstacle->ahead); VEL(BACK);
  IF (AND(OR(obstacle->right, obstacle->left), NOT(obstacle->ahead))); VEL(SLOW);
  IF (NOT(OR(OR(obstacle->right,obstacle->left), obstacle->ahead))); VEL(FAST);
  // clang-format on
}

// Wrapper for calling all the specific fuzzy.h-library functionality, using a ruleset as a parameter
Speed rulesetController(void (*ruleset)(GeneralPredicatesDouble*), GeneralPredicatesDouble* predicates) {
  // Clear the fuzzy sets
  ClearFSet(f_set_vlin);
  ClearFSet(f_set_vrot);

  // Execute the given ruleset using the supplied predicate
  (*ruleset)(predicates);

  // Convert the fuzzy sets to values in [0,1]
  double vel, rot;
  DeFuzzify(f_set_vlin, 4, &vel);  // Defuzzify and set rot/vel
  DeFuzzify(f_set_vrot, 3, &rot);

  // Calculate radial speed
  double vt = ResponseToVel(vel);
  double omega = ResponseToRot(rot);

  // Run inverse kinematics to calculate specific wheel speed
  Speed toBeSet;
  toBeSet.l = (int)(vt + (ROBOT_DIAMETER / 2) * omega);
  toBeSet.r = (int)(vt - (ROBOT_DIAMETER / 2) * omega);

  // Return this speed, make sure, that at least one wheel is fast enough
  return normalizeSpeed(toBeSet);
}

// A controller in our standard controller format using the gotoRuleset
Speed gotoRulesetController(PostureDouble* current, PostureDouble* goal, void* additionalInformation, int* status) {
  // calculate predicates
  DirectionPredicatesDouble position;
  GeneralPredicatesDouble predicates;
  predicates.position = &position;
  predicates.obstacle = NULL;

  calculateGoalPredicates(&position, current, goal);
  Speed speed;

  // set status to 1 if done, else calculate movement using ruleset
  if (position.here > 0.8) {
    speed.l = 0;
    speed.r = 0;
    *status = 1;
  } else {
    speed = rulesetController(&gotoRuleset, &predicates);
    *status = 0;
  }

  return speed;
}

// A controller in our standard gotoController-format using the gotoAvoidingObstacles ruleset
Speed gotoRulesetAvoidingObstaclesController(PostureDouble* current, PostureDouble* goal, void* additionalInformation, int* status) {
  Sensors* ir = (Sensors*)additionalInformation;

  // calculate predicates
  DirectionPredicatesDouble position, obstacle;
  GeneralPredicatesDouble predicates;
  predicates.position = &position;
  predicates.obstacle = &obstacle;

  calculateGoalPredicates(&position, current, goal);
  calculateObstaclePredicates(&obstacle, ir);

  Speed speed;

  // set status to 1 if done, else calculate movement using ruleset
  if (position.here > 0.5) {
    speed.l = 0;
    speed.r = 0;
    *status = 1;
  } else {
    speed = rulesetController(&gotoAvoidingObstaclesRuleset, &predicates);
    *status = 0;
  }

  return speed;
}

void printDirectionPredicate(DirectionPredicatesDouble* predicates) {
  printf("[left=%.2f, right=%.2f, ahead=%.2f, here=%.2f]\n", predicates->left, predicates->right, predicates->ahead, predicates->here);
}

void testRuleset(void (*ruleset)(GeneralPredicatesDouble*)) {
  DirectionPredicatesDouble obstacle, position;
  GeneralPredicatesDouble predicates;
  predicates.obstacle = &obstacle;
  predicates.position = &position;

  for (int i = 255; i >= 0; i--) {
    obstacle.left = (i / 1) % 2;
    obstacle.right = (i / 2) % 2;
    obstacle.ahead = (i / 4) % 2;
    obstacle.here = (i / 8) % 2;
    position.left = (i / 16) % 2;
    position.right = (i / 32) % 2;
    position.ahead = (i / 64) % 2;
    position.here = (i / 128) % 2;

    if ((position.ahead == 0 && position.left == 0 && position.right == 0) || (position.left == 1 && position.right == 1)) {
      continue;
    }

    if ((obstacle.here == 1 && obstacle.left == 0 && obstacle.ahead == 0 && obstacle.right == 0) ||
        (obstacle.here == 0 && (obstacle.left == 1 || obstacle.right == 1 || obstacle.ahead == 1))) {
      continue;
    }

    printf("===================================\n");

    printf("position = ");
    printDirectionPredicate(&position);
    printf("obstacle = ");
    printDirectionPredicate(&obstacle);

    ClearFSet(f_set_vlin);
    ClearFSet(f_set_vrot);

    (*ruleset)(&predicates);

    double vel, rot;
    DeFuzzify(f_set_vlin, 4, &vel);
    DeFuzzify(f_set_vrot, 3, &rot);

    printf("vel = %.2f, rot = %.2f\n", vel, rot);
  }
}
