#ifndef LIBFUZZY_H_INCLUDED
#define LIBFUZZY_H_INCLUDED

#include "fuzzy.h"
#include "interface.h"
#include "libgoalcontroller.h"
#include "libposition.h"
#include "simulatorhack.h"

#define FULL_DANGER 400
#define NO_DANGER 200

typedef struct {
  FPred left;
  FPred right;
  FPred ahead;
  FPred here;
} DirectionPredicatesDouble;

typedef struct {
  DirectionPredicatesDouble* position;
  DirectionPredicatesDouble* obstacle;
} GeneralPredicatesDouble;

void calculateGoalPredicates(DirectionPredicatesDouble* predicates, PostureDouble* current, PostureDouble* goal);
void calculateObstaclePredicates(DirectionPredicatesDouble* predicates, Sensors* ir);

void gotoAvoidingObstaclesRuleset(GeneralPredicatesDouble* predicates);

void gotoRuleset(GeneralPredicatesDouble* predicates);
void avoidObstaclesRuleset(GeneralPredicatesDouble* predicates);

void improvedGotoRuleset(GeneralPredicatesDouble* predicates);
void improvedAvoidObstaclesRuleset(GeneralPredicatesDouble* predicates);

Speed rulesetController(void (*ruleset)(GeneralPredicatesDouble*), GeneralPredicatesDouble* predicates);
Speed gotoRulesetController(PostureDouble* current, PostureDouble* goal, void* additionalInformation, int* status);
Speed gotoRulesetAvoidingObstaclesController(PostureDouble* current, PostureDouble* goal, void* additionalInformation, int* status);

void testRuleset(void (*ruleset)(GeneralPredicatesDouble*));
void printDirectionPredicate(DirectionPredicatesDouble* predicates);

#endif
