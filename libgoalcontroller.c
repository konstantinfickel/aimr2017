#include "libgoalcontroller.h"
#include <math.h>

// Helper function for normalizeSpeed(), scales every component by
// toValue/fromValue.
Speed normalizeSpeedTo(Speed toBeNormalized, int fromValue, int toValue) {
  Speed normalizedSpeed;
  normalizedSpeed.l = (int)floor(((double)(toBeNormalized.l)) / (((double)fromValue / toValue)));
  normalizedSpeed.r = (int)floor(((double)(toBeNormalized.r)) / (((double)fromValue / toValue)));
  return normalizedSpeed;
}

// Makes sure, that highest wheel speed is in between 1000 and 200, so that the
// robot is moving when it's supposed to.
Speed normalizeSpeed(Speed toBeNormalized) {
  int maximumWheelSpeed = (int)fmax(fabs(toBeNormalized.l), fabs(toBeNormalized.r));
  if (maximumWheelSpeed == 0) {
    return toBeNormalized;
  } else if (maximumWheelSpeed <= 400) {
    return normalizeSpeedTo(toBeNormalized, maximumWheelSpeed, 400);
  } else if (maximumWheelSpeed <= 1000) {
    return toBeNormalized;
  } else {
    return normalizeSpeedTo(toBeNormalized, maximumWheelSpeed, 1000);
  }
}

// Tracks the given path using the goal controller
Speed trackPath(PostureDouble* posture, PathDouble* path, Speed (*goalController)(PostureDouble*, PostureDouble*, void*, int*),
                double acceptanceRadius, int* nextPoint, void* additionalInformation, int* status) {
  // Select next point in path, which is not within <TRACK_MINTRACKDISTANCE>cm
  while (*nextPoint + 1 < path->size) {
    if (calculateDistance(posture, &(path->postures[*nextPoint])) > acceptanceRadius) {
      break;
    }
    (*nextPoint) += 1;
  }

  // Call movement function
  int goalControllerStatus = 0;
  Speed speed = (*goalController)(posture, &(path->postures[*nextPoint]), additionalInformation, &goalControllerStatus);

  (*status) = 0;

  // Interpret return value, return Speed
  if (goalControllerStatus == 1 && *nextPoint + 1 >= path->size) {
    (*status) = 1;
  }
  return speed;
}

// Follows the gradient using the goal controller
Speed followGradient(PostureDouble* posture, Map* gradientMap, Speed (*goalController)(PostureDouble*, PostureDouble*, void*, int*),
                     double acceptanceRadius, void* additionalInformation, int* status) {
  Cell goalCell = coordinatesToCell(FindGoal(gradientMap));

  // Cell the robot will try to get to (starting with current cell)
  Cell lastCell = postureToCell(*posture);
  PostureDouble lastPosture = cellToPosture(lastCell);

  // Follow the gradient until it reaches the goal or it is more than acceptanceRadius mm away
  while (!(cellEquals(&lastCell, &goalCell) || calculateDistance(posture, &lastPosture) > acceptanceRadius)) {
    int bestValue = INT_MAX;
    Cell bestCell;

    List* neighbours = getNeighbours(lastCell);

    while (!IsListEmpty(neighbours)) {
      Cell neighbour = Pop(neighbours);
      int neighbourValue = GetCellState(gradientMap, neighbour.i, neighbour.j);
      if (neighbourValue >= 0 && neighbourValue < bestValue) {
        bestValue = neighbourValue;
        bestCell.i = neighbour.i;
        bestCell.j = neighbour.j;
      }
    }

    FreeList(neighbours);

    // If there is no way to the goal, just set the tracked point to the goal coordiante
    if (bestValue == INT_MAX) {
      lastCell = goalCell;
      lastPosture = cellToPosture(lastCell);
      break;
    };

    // Update last cell for the next iteration
    lastCell = bestCell;
    lastPosture = cellToPosture(lastCell);
  }

  // Call movement function
  int goalControllerStatus = 0;
  Speed speed = (*goalController)(posture, &lastPosture, additionalInformation, &goalControllerStatus);

  (*status) = 0;

  // Interpret return value, return Speed
  if (goalControllerStatus == 1 && cellEquals(&lastCell, &goalCell)) {
    (*status) = 1;
  }
  return speed;
}

// Way to set the speed of the robot without having to destructurize the
// Speed-Structure
void setSpeed(Speed speed) { SetSpeed(speed.l, speed.r); }
