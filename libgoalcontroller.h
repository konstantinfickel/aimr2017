#ifndef LIBGOALCONTROLLER_H_INCLUDED
#define LIBGOALCONTROLLER_H_INCLUDED

#include "interface.h"
#include "libpath.h"
#include "libpathplanning.h"
#include "libposition.h"
#include "maps.h"
#include "simulatorhack.h"

#define TRACK_MINTRACKDISTANCE 85.0

Speed normalizeSpeed(Speed toBeNormalized);
Speed trackPath(PostureDouble* posture, PathDouble* path, Speed (*goalController)(PostureDouble*, PostureDouble*, void*, int*),
                double acceptanceRadius, int* nextPoint, void* additionalInformation, int* status);
Speed followGradient(PostureDouble* posture, Map* gradient, Speed (*goalController)(PostureDouble*, PostureDouble*, void*, int*),
                     double acceptanceRadius, void* additionalInformation, int* status);
void setSpeed(Speed speed);

#endif
