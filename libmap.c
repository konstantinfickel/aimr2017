#include "libmap.h"

// If posture outside map, force posture to closes location inside map borders
void forceIntoMap(Map* map, PostureDouble* current) {
  if (current->x < 0.5 * GRID_SIZE) {
    // This value is slightly higher so that the postureToCell will definitly output a valid grid cell
    current->x = 0.55 * GRID_SIZE;
  }
  if (current->y < 0.5 * GRID_SIZE) {
    current->y = 0.55 * GRID_SIZE;
  }
  if (current->x > ((double)map->height - 1.5) * GRID_SIZE) {
    current->x = (map->height - 1.55) * GRID_SIZE;
  }
  if (current->y > ((double)map->width - 1.5) * GRID_SIZE) {
    current->y = (map->width - 1.55) * GRID_SIZE;
  }
}

// Checks, if grid cell is outside the given map (or inside the border)
bool isOutsideMap(Coordinates coordinates, Map* map) {
  return (coordinates.i <= 0 || coordinates.i >= (map->width - 1) || coordinates.j <= 0 || (coordinates.j >= (map->height - 1)));
}

// Functions for cloning, flipping and copying maps
Map* map_clone(Map* map) {
  Map* clone = CreateEmptyMap(map->width, map->height, map->max_value);

  map_copy(map, clone);

  return clone;
}

void map_copy(Map* source, Map* target) {
  if (source->width != target->width || source->height != target->height) {
    return;
  }

  for (int i = 0; i < source->height; i++) {
    for (int j = 0; j < target->width; j++) {
      ChangeCellState(target, i, j, GetCellState(source, i, j));
    }
  }
}

Map* map_flipHorizontally(Map* map) {
  Map* rotatedMap = CreateEmptyMap(map->width, map->height, map->max_value);
  for (int i = 0; i < map->height; i++) {
    for (int j = 0; j < map->width; j++) {
      ChangeCellState(rotatedMap, i, j, GetCellState(map, map->height - 1 - i, j));
    }
  }
  return rotatedMap;
}

// functions for printing the map
bool list_contains(List* list, int i, int j) {
  if (IsListEmpty(list)) return false;

  ListElement* element = list->head;
  do {
    if (element->data.i == i && element->data.j == j) return true;
    element = element->next;
  } while (element != NULL);

  return false;
}

void map_print(Map* map, List* path, Coordinates* coordinates) {
  if (map == NULL) return;

  for (int i = map->height - 1; i >= 0; i--) {
    for (int j = 0; j < map->width; j++) {
      char str[12];
      bool partOfPath = path != NULL ? list_contains(path, i, j) : false;
      bool currentPosition = i == coordinates->i && j == coordinates->j;
      MapToSymbol(map->map[i][j], str);
      printf("%s%s%s%s", currentPosition ? COLOR_GREEN : (partOfPath ? COLOR_YELLOW : ""), str, COLOR_RESET,
             (j < map->width - 1) ? " " : "\n");
    }
  }
}
