#ifndef LIBMAP_H_INCLUDED
#define LIBMAP_H_INCLUDED

#include "simulatorhack.h"

#include "libcoordinates.h"
#include "libpath.h"
#include "libposition.h"
#include "lists.h"
#include "maps.h"

#if defined(__linux__)
#define COLOR_RESET "\x1B[0m"
#define COLOR_YELLOW "\x1B[33m"
#define COLOR_GREEN "\x1B[32m"
#else
#define COLOR_RESET ""
#define COLOR_YELLOW ""
#define COLOR_GREEN ""
#endif

void forceIntoMap(Map* map, PostureDouble* current);
bool isOutsideMap(Coordinates coordinates, Map* map);

Map* map_clone(Map* map);
void map_copy(Map* source, Map* target);
Map* map_flipHorizontally(Map* map);

bool list_contains(List* list, int i, int j);
void map_print(Map* map, List* path, Coordinates* coordinates);

#endif
