#include "libmapupdate.h"
#include "stdlib.h"

// Our sensor calibration
#define PERCENT_TO_DISTANCE_LENGTH 12
double percentageToDistance[] = {0.9872, 0.4232, 0.1341, 0.0667, 0.0381, 0.0234, 0.0160, 0.0120, 0.0088, 0.0067, 0.0045, 0.0035};

#ifndef USING_SIMULATOR
int lowestValueOfSensor[] = {221, 195, 208, 165, 105, 162, 203, 192};

int highestValueOfSensor[] = {3965, 3945, 3950, 3945, 3949, 3978, 3953, 3955};
#else
int lowestValueOfSensor[] = {10, 10, 10, 10, 10, 10, 10, 10};

int highestValueOfSensor[] = {1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000};
#endif

#define SENSOR_NUMBER 8

// Angle of the sensors
double sensorAngle[] = {-0.29999999996640769, -0.87142857131499862, -1.5707963267948966, -2.5844497965195483,
                        2.5844497965195483,   1.5707963267948966,   0.87142857131499862, 0.29999999996640769};

// Distance of the robot center to the robot
#define SENSOR_DISTANCE 35.0

// Determines the sensor number of the next sensor on the left for a specific angle
int findLeftSensor(double angleDifference, int sensorNum, double* sensorAngle) {
  for (int i = 0; i < sensorNum; i++) {
    if (pmod(sensorAngle[i], 2 * PI) < pmod(angleDifference, 2 * PI)) {
      return i;
    }
  }
  return 0;
}

// Calculates "inverse" weighted average
double weightedAverageByAngleDistance(double a, double aWeight, double b, double bWeight) {
  return (a * bWeight + b * aWeight) / (aWeight + bWeight);
}

// Determines the distance from the IR-value, using the calibration values specified above
double obstacleDistanceForSensor(int irValue, int sensor) {
  double normalizedIrValue = (double)(irValue - lowestValueOfSensor[sensor]) / (highestValueOfSensor[sensor] - lowestValueOfSensor[sensor]);
  int upperBoundForDistance;
  for (upperBoundForDistance = 0; upperBoundForDistance < PERCENT_TO_DISTANCE_LENGTH; upperBoundForDistance++) {
    if (normalizedIrValue > percentageToDistance[upperBoundForDistance]) break;
  }

  if (upperBoundForDistance == 0) return 0;

  return 10 * weightedAverageByAngleDistance(upperBoundForDistance - 1, percentageToDistance[upperBoundForDistance - 1] - normalizedIrValue,
                                             upperBoundForDistance, normalizedIrValue - percentageToDistance[upperBoundForDistance]);
}

double calculateAttendanceRadius(double angle) {
  if (fabs(angle) < 0.5 * PI) {
    return ATTENDANCE_RADIUS;
  } else {
    return ATTENDANCE_RADIUS - 40 * (fabs(angle) / (0.5 * PI) - 1);
  }
}

bool updateCell(Map* map, PostureDouble* posture, Sensors* ir, Cell cell) {
  // Calculate posture of cell center
  PostureDouble cellPosture = cellToPosture(cell);
  // Get distance to cell
  double cellDistance = calculateDistance(posture, &cellPosture);
  // Get current cell state
  int cellState = GetCellState(map, cell.i, cell.j);

  // Shortcut for cells, that won't be changed either way
  if (cellDistance > ATTENDANCE_RADIUS + GRID_SIZE || (cellState != MAP_UNKNOWN && cellState != MAP_OBSTACLE)) {
    return false;
  }

  // Get angle of sensor
  double absoluteCellDirectionAngle = calculateAngle(posture, &cellPosture);
  double relativeCellDirectionAngle = calculateAngleDifference(absoluteCellDirectionAngle, posture->theta);

  // Find adjasent sensors to cell direction
  int leftSensor = findLeftSensor(relativeCellDirectionAngle, SENSOR_NUMBER, sensorAngle);
  int rightSensor = (leftSensor - 1 + SENSOR_NUMBER) % SENSOR_NUMBER;

  // Calculates adjasent sensor distances
  double leftSensorDistance = obstacleDistanceForSensor(ir->sensor[leftSensor], leftSensor) + SENSOR_DISTANCE;
  double rightSensorDistance = obstacleDistanceForSensor(ir->sensor[rightSensor], rightSensor) + SENSOR_DISTANCE;
  // Calculates weight for average, which is the angle distance to cell
  double leftSensorWeight = fabs(calculateAngleDifference(relativeCellDirectionAngle, sensorAngle[leftSensor]));
  double rightSensorWeight = fabs(calculateAngleDifference(relativeCellDirectionAngle, sensorAngle[rightSensor]));

  // Calculate value of pseudo-sensor
  double obstacleInCellDirectionDistance =
      weightedAverageByAngleDistance(leftSensorDistance, leftSensorWeight, rightSensorDistance, rightSensorWeight);

  // Calculates attendance radius
  double attendanceRadius = calculateAttendanceRadius(relativeCellDirectionAngle);

  if (cellState == MAP_OBSTACLE) {
    if (attendanceRadius > cellDistance + GRID_SIZE / 2 && obstacleInCellDirectionDistance > cellDistance + GRID_SIZE / 2) {
      // Remove obstacle, if it is possible to view through cell
      ChangeCellState(map, cell.i, cell.j, MAP_UNKNOWN);
      return true;
    }
  } else if (cellState == MAP_UNKNOWN) {
    if (obstacleInCellDirectionDistance < attendanceRadius && (obstacleInCellDirectionDistance > cellDistance - GRID_SIZE / 2) &&
        (obstacleInCellDirectionDistance < cellDistance + GRID_SIZE / 2)) {
      // Set obstacle, if encountered inside cell
      ChangeCellState(map, cell.i, cell.j, MAP_OBSTACLE);
      return true;
    }
  }

  return false;
}

bool updateMap(Map* map, PostureDouble* posture, Sensors* ir) {
  bool updated = false;
  // Update every cell
  for (int i = 0; i < map->height; i++) {
    for (int j = 0; j < map->width; j++) {
      Cell cell;
      cell.i = i;
      cell.j = j;
      updated |= updateCell(map, posture, ir, cell);
    }
  }
  // True, if any cell has been updated
  return updated;
}
