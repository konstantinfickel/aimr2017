#ifndef LIBMAPUPDATE_H_INCLUDED
#define LIBMAPUPDATE_H_INCLUDED

#include "libgoalcontroller.h"
#include "libpathplanning.h"
#include "libposition.h"
#include "lists.h"
#include "maps.h"
#include "simulatorhack.h"

#define ATTENDANCE_RADIUS 95

int findLeftSensor(double angleDifference, int sensorNum, double* sensorAngle);
double weightedAverageByAngleDistance(double a, double aWeight, double b, double bWeight);
double obstacleDistanceForSensor(int irValue, int sensor);
double calculateAttendanceRadius(double angle);
bool updateCell(Map* map, PostureDouble* posture, Sensors* ir, Cell cell);
bool updateMap(Map* map, PostureDouble* posture, Sensors* ir);

#endif