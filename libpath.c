#include "libpath.h"
#include <stdio.h>

PathDouble* path_create() {
  PathDouble* path = (PathDouble*)malloc(sizeof(PathDouble));
  path->size = 0;
  path->capacity = PATHDOUBLE_MINIMAL_ALLOCATED_SIZE;
  path->postures = malloc(sizeof(PostureDouble) * path->capacity);
  return path;
}

void path_free(PathDouble* path) {
  free(path->postures);
  free(path);
}

void path_doubleCapacityIfFull(PathDouble* path) {
  if (path->size >= path->capacity) {
    path->capacity *= 2;
    path->postures = (PostureDouble*)realloc(path->postures, sizeof(PostureDouble) * path->capacity);
  }
}

void path_append(PathDouble* path, PostureDouble* posture) {
  path_doubleCapacityIfFull(path);
  memcpy(&(path->postures[path->size]), posture, sizeof(PostureDouble));
  path->size += 1;
}

void path_print_posture(int id, PostureDouble* posture) {
  printf("#%d = (x=%.2f, y=%.2f, theta=%.2f)\n", id, posture->x, posture->y, posture->theta * (360 / (2 * PI)));
}

void path_print(PathDouble* path) {
  for (int i = 0; i < path->size; i++) {
    path_print_posture(i, &(path->postures[i]));
  }
}

int path_save(char* filename, PathDouble* path) {
  FILE* toBeSavedInto = fopen(filename, "w");
  for (int i = 0; i < path->size; i++) {
    fprintf(toBeSavedInto, "%f,%f,%f\n", (path->postures + i)->x, (path->postures + i)->y, (path->postures + i)->theta);
  }
  fclose(toBeSavedInto);
  return 0;
}

PathDouble* path_load(char* filename) {
  PathDouble* loadedPath = path_create();
  FILE* toBeLoadedFrom = fopen(filename, "r");
  PostureDouble loadedPosture;
  while (fscanf(toBeLoadedFrom, "%lf,%lf,%lf\n", &(loadedPosture.x), &(loadedPosture.y), &(loadedPosture.theta)) >= 3) {
    loadedPosture.absoluteTheta = loadedPosture.theta;
    path_append(loadedPath, &loadedPosture);
  }
  fclose(toBeLoadedFrom);
  return loadedPath;
}
