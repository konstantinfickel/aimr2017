#ifndef LIBPATH_H_INCLUDED
#define LIBPATH_H_INCLUDED

#include "libposition.h"

// Data structure to symbolize a path movement
typedef struct {
  // The array of postures the robot will have to visitg
  PostureDouble* postures;
  int size;
  int capacity;
} PathDouble;

#define PATHDOUBLE_MINIMAL_ALLOCATED_SIZE 5

PathDouble* path_create();
void path_free(PathDouble* path);
void path_doubleCapacityIfFull(PathDouble* path);
void path_append(PathDouble* path, PostureDouble* posture);
void path_print(PathDouble* path);
int path_save(char* filename, PathDouble* path);
PathDouble* path_load(char* filename);

#endif
