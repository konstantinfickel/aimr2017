#include "libpathplanning.h"

PathDouble* listToPath(List* list) {
  PathDouble* toBeReturned = path_create();

  if (IsListEmpty(list)) return toBeReturned;
  ListElement* element = list->head;
  do {
    PostureDouble newPosture = cellToPosture(element->data);
    path_append(toBeReturned, &newPosture);
    element = element->next;
  } while (element != NULL);

  return toBeReturned;
}

List* getNeighbours(Cell cell) {
  List* neighbours = CreateList(FIFO);
  for (int j = -1; j <= 1; j++) {
    for (int i = -1; i <= 1; i++) {
      if ((i != 0 || j != 0) && (i == 0 || j == 0)) {
        Cell neighbour;
        neighbour.i = cell.i - i;
        neighbour.j = cell.j - j;
        Push(neighbours, neighbour);
      }
    }
  }
  return neighbours;
}

void addCellToList(Map* map, List* toBeProcessed, int i, int j, int distance, Cell* goal, enum Algorithm algorithm) {
  switch (GetCellState(map, i, j)) {
    case MAP_UNKNOWN:
      ChangeCellState(map, i, j, distance);
    case MAP_START:;  // Hack, because case can not start with declaration
      Cell addCell;
      addCell.i = i;
      addCell.j = j;
      switch (algorithm) {
        case ASTAR:
          addCell.h_value = calculateManhattanDistance(goal, &addCell) + distance;
          break;
        case HEURISTIC:
          addCell.h_value = calculateManhattanDistance(goal, &addCell);
          break;
        default:
          addCell.h_value = 0.0;
      }

      Push(toBeProcessed, addCell);
      return;
    default:
      return;
  }
}

void searchMap(Map* map, enum Algorithm algorithm, bool finishDiscovering) {
  List* toBeProcessed;

  switch (algorithm) {
    case HEURISTIC:
    case ASTAR:
      toBeProcessed = CreateList(SORTED);
      break;
    case DFS:
      toBeProcessed = CreateList(LIFO);
      break;
    default:
      toBeProcessed = CreateList(FIFO);
  }

  Push(toBeProcessed, coordinatesToCell(FindGoal(map)));
  Cell start = coordinatesToCell(FindStart(map));

  while (!IsListEmpty(toBeProcessed)) {
    Cell cell = Pop(toBeProcessed);
    if (cellEquals(&cell, &start)) {
      if (finishDiscovering)
        continue;
      else
        break;
    }

    int distance = GetCellState(map, cell.i, cell.j) + 1;

    List* neighbours = getNeighbours(cell);
    while (!IsListEmpty(neighbours)) {
      Cell neighbour = Pop(neighbours);
      addCellToList(map, toBeProcessed, neighbour.i, neighbour.j, distance, &start, algorithm);
    }
    FreeList(neighbours);
  }

  FreeList(toBeProcessed);
}

void calculateGradientMap(Map* map, Coordinates start, Coordinates goal, enum Algorithm algorithm) {
  Cell goalCell = coordinatesToCell(goal);

  ChangeCellState(map, start.i, start.j, MAP_START);
  ChangeCellState(map, goal.i, goal.j, MAP_GOAL);

  if (start.i == goal.i && start.j == goal.j) {
    return;
  }

  searchMap(map, algorithm, true);
}

List* calculateBestPath(Map* map, Coordinates start, Coordinates goal, enum Algorithm algorithm) {
  List* bestPath = CreateList(FIFO);

  Cell lastPosition = coordinatesToCell(start);
  Push(bestPath, lastPosition);

  Cell goalCell = coordinatesToCell(goal);

  ChangeCellState(map, start.i, start.j, MAP_START);
  ChangeCellState(map, goal.i, goal.j, MAP_GOAL);

  if (start.i == goal.i && start.j == goal.j) {
    Push(bestPath, goalCell);
    return bestPath;
  }

  searchMap(map, algorithm, false);

  do {
    int bestValue = INT_MAX;
    Cell bestCell;

    List* neighbours = getNeighbours(lastPosition);

    while (!IsListEmpty(neighbours)) {
      Cell neighbour = Pop(neighbours);
      int neighbourValue = GetCellState(map, neighbour.i, neighbour.j);
      if (neighbourValue >= 0 && neighbourValue < bestValue) {
        bestValue = neighbourValue;
        bestCell.i = neighbour.i;
        bestCell.j = neighbour.j;
      }
    }

    FreeList(neighbours);

    // If there is no way to the goal
    if (bestValue == INT_MAX) {
      Push(bestPath, goalCell);
      return bestPath;
    };

    Push(bestPath, bestCell);
    lastPosition = bestCell;
  } while (!cellEquals(&lastPosition, &goalCell));

  return bestPath;
}
