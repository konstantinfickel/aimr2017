#ifndef LIBPATHPlANNING_H_INCLUDED
#define LIBPATHPlANNING_H_INCLUDED

#include "simulatorhack.h"

#include "libcoordinates.h"
#include "libmap.h"
#include "libpath.h"
#include "libposition.h"
#include "limits.h"
#include "lists.h"
#include "maps.h"

enum Algorithm { ASTAR, BFS, DFS, HEURISTIC };

PathDouble* listToPath(List* list);
List* getNeighbours(Cell cell);
void calculateGradientMap(Map* map, Coordinates start, Coordinates goal, enum Algorithm algorithm);
List* calculateBestPath(Map* map, Coordinates start, Coordinates goal, enum Algorithm algorithm);

#endif
