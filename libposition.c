#include "libposition.h"
#include <math.h>
#include <stdio.h>

// Calculates the difference in every components between both steps structures
Steps stepsDifference(Steps a, Steps b) {
  Steps result;
  result.l = a.l - b.l;
  result.r = a.r - b.r;
  return result;
}

// signum-function
double fsgn(double x) {
  if (x > 0)
    return 1;
  else if (x < 0)
    return -1;
  else
    return 0;
}

// Calculates Angle between two positions
double calculateAngle(PostureDouble* a, PostureDouble* b) {
  double up = (b->x - a->x);
  double right = (b->y - a->y);

  return atan2(right, up);
}

// Calculate Distance between two positions
double calculateDistance(PostureDouble* a, PostureDouble* b) { return sqrt(pow(a->x - b->x, 2) + pow(a->y - b->y, 2)); }

// Calculates distance between two angles
double calculateAngleDifference(double source, double target) {
  if (fabs(source - target) < PI)
    return target - source;
  else
    return fsgn(source - target) * (2 * PI - fabs(source - target));
}

// Converts Steps to StepsDouble, applies correction factor for position
// estimation and converts to mm
StepsDouble enc2mmDouble(Steps steps) {
  StepsDouble result;
  result.l = MM_PER_PULSE_L * CORRECTION_FACTOR_L * (double)steps.l;
  result.r = MM_PER_PULSE_R * CORRECTION_FACTOR_R * (double)steps.r;
  return result;
}

// Modification of fmod that always outputs positive values
double pmod(double value, double quotient) { return (value >= 0) ? fmod(value, quotient) : quotient + fmod(value, quotient); }

// exercise 1b-1, implementation of the position estimation algorithm
PostureDouble calculatePosture(PostureDouble lastPosture, Steps lastSteps, Steps newSteps) {
  Steps stepsDone = stepsDifference(newSteps, lastSteps);
  StepsDouble mmMoved = enc2mmDouble(stepsDone);

  double d = (mmMoved.l + mmMoved.r) / 2;
  double delta = (mmMoved.l - mmMoved.r) / ROBOT_DIAMETER;
  double d_x = d * cos(delta / 2);
  double d_y = d * sin(delta / 2);

  PostureDouble newPosture;
  newPosture.x = lastPosture.x + d_x * cos(lastPosture.theta) - d_y * sin(lastPosture.theta);
  newPosture.y = lastPosture.y + d_x * sin(lastPosture.theta) + d_y * cos(lastPosture.theta);
  newPosture.theta = pmod(lastPosture.theta + delta, 2 * PI);
  newPosture.absoluteTheta = lastPosture.absoluteTheta + delta;
  return newPosture;
}

// Outputs given posture
void print_posture(PostureDouble* posture) {
  printf("x = %.2f, y = %.2f, theta = %.2f, absoluteTheta = %.2f\n", posture->x, posture->y, posture->theta * (360 / (2 * PI)),
         posture->absoluteTheta * (360 / (2 * PI)));
}
