#ifndef LIBPOSITION_H_INCLUDED
#define LIBPOSITION_H_INCLUDED

/* LIBPOSITION v0.1 */

#include "interface.h"

// Correction factors for the position estimation
#define CORRECTION_FACTOR_L 0.9935
#define CORRECTION_FACTOR_R 0.9935

// Custom data structure for calculations with double-precisions
typedef struct {
  // Distance gone forwards relative to starting location
  double x;
  // Distance gone right relative to starting location
  double y;
  // Angle in [0,2*PI) relative to starting location
  double theta;
  // Like theta, but without cutting of after getting above 2*PI or below 0
  double absoluteTheta;
} PostureDouble;

// Exactly as steps / speed, but with doubles instead of integers
typedef struct {
  double l;
  double r;
} StepsDouble, SpeedDouble;

Steps stepsDifference(Steps a, Steps b);
double fsgn(double x);
double calculateAngle(PostureDouble* a, PostureDouble* b);
double calculateDistance(PostureDouble* a, PostureDouble* b);
double calculateAngleDifference(double source, double target);
StepsDouble enc2mmDouble(Steps steps);
double pmod(double value, double quotient);
PostureDouble calculatePosture(PostureDouble lastPosture, Steps lastSteps, Steps newSteps);
void print_posture(PostureDouble* posture);

#endif
