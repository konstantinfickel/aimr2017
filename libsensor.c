#include "libsensor.h"

#ifndef USING_SIMULATOR
// Makes corresponding LED for Sensor light up, if light above threshold
void showDistanceWithLedRing(Sensors sensors, int threshold) {
  bool proximity[RING_LEDS_NUM];
  for (int i = 0; i < 8; i++) {
    proximity[i] = sensors.sensor[i] > threshold;
  }
  SetRingLED(proximity);
}

#else
void showDistanceWithLedRing(Sensors sensors, int threshold) {}
#endif
