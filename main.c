#include <stdlib.h>
// required for console output
#include <stdio.h>
// required to parse enum to string (using string comparisons)
#include <string.h>
// command line parsing library
#include <argp.h>

// The time the robot pauses between setting the speed again by the main loop
#define SAMPLING_TIME 100

// Our content to the /run-folder of the project can be found online at:
// https://bitbucket.org/konstantinfickel/aimr2017/src

#include "interface.h"

#include "libfuzzy.h"
#include "libgoalcontroller.h"
#include "libmapupdate.h"
#include "libpath.h"
#include "libpathplanning.h"
#include "libsensor.h"

enum Architecture { SENSE_PLAN_ACT, REACTIVE, HYBRID, GRADIENT };

// Calculates path based on searchMap and stores it into path.
void recalculatePath(Map* searchMap, PathDouble** path, List** pointsList, Coordinates start, Coordinates goal, enum Algorithm algorithm) {
  if (*pointsList != NULL) {
    FreeList(*pointsList);
  }
  *pointsList = calculateBestPath(searchMap, start, goal, algorithm);
  if (*path != NULL) {
    path_free(*path);
  }
  *path = listToPath(*pointsList);
}

// Calculeates gradient map based on map
void recalculateGradientMap(Map* map, Coordinates start, Coordinates goal, enum Algorithm algorithm) {
  calculateGradientMap(map, start, goal, algorithm);
}

// Function running the main control loop
void runControlLoop(char* robotNumber, PostureDouble startPosture, PostureDouble goalPosture, enum Algorithm searchAlgorithm,
                    enum Architecture architecture, char* mapInputPath, char* mapOutputPath, char* trackOutputFile) {
  epuck(robotNumber);
  ClearSteps();

  printf("Starting...\n");

  Map* obstacleMap;

  if (mapInputPath != NULL) {
    Map* mapFlipped = CreateMapFromFile(SYM, mapInputPath);
    obstacleMap = map_flipHorizontally(mapFlipped);
    FreeMap(mapFlipped);
  } else {
    obstacleMap = CreateBorderedMap(13, 13, -1);
  }

  // Create start and goal point
  Coordinates goal = postureToCoordinates(goalPosture);
  PostureDouble current = startPosture;

  // Prepare searchMap for the first map
  Map* searchMap = map_clone(obstacleMap);
  List* list = NULL;
  PathDouble* path = NULL;

  Coordinates currentCoordinates = postureToCoordinates(current);

  // If command line starting or goal position maps to cell outside the map, exit
  if (isOutsideMap(goal, searchMap)) {
    printf("Error: Goal coordinates have to be inside the map!\n");
    FreeMap(obstacleMap);
    FreeMap(searchMap);
    exit(1);
  } else if (isOutsideMap(currentCoordinates, searchMap)) {
    printf("Error: Start coordinates have to be inside the map!\n");
    FreeMap(obstacleMap);
    FreeMap(searchMap);
    exit(1);
  }

  // Calculate Path based on the input map
  switch (architecture) {
    case HYBRID:
    case SENSE_PLAN_ACT:
      recalculatePath(searchMap, &path, &list, currentCoordinates, goal, searchAlgorithm);
      break;
    case REACTIVE:
      break;
    case GRADIENT:
      recalculateGradientMap(searchMap, currentCoordinates, goal, searchAlgorithm);
  }

  map_print(searchMap, list, &currentCoordinates);

  // Initialize Variables for main loop
  Steps lastSteps;
  Steps newSteps = GetSteps();
  int nextPoint = 0;

  // Path the steps will be recorded in
  PathDouble* trackedPath = path_create();

  // Main control loop with tick counting variable
  for (int i = 0; true; i++) {
    // Read wheel increments
    lastSteps = newSteps;
    newSteps = GetSteps();

    // Predict new position
    current = calculatePosture(current, lastSteps, newSteps);
    forceIntoMap(obstacleMap, &current);
    path_append(trackedPath, &current);

    // read IR-values
    Sensors ir = GetIR();

    printf("\n");
    currentCoordinates = postureToCoordinates(current);

    // Update map, if required by architecture
    if (architecture == HYBRID && updateMap(obstacleMap, &current, &ir)) {
      // Recalculate path, if something in the map changed
      map_copy(obstacleMap, searchMap);
      recalculatePath(searchMap, &path, &list, currentCoordinates, goal, searchAlgorithm);
      nextPoint = 0;
    } else if (architecture == GRADIENT && updateMap(obstacleMap, &current, &ir)) {
      // Recalculate gradient, if something in the map changed
      map_copy(obstacleMap, searchMap);
      recalculateGradientMap(searchMap, currentCoordinates, goal, searchAlgorithm);
      nextPoint = 0;
    }

    map_print(searchMap, list, &currentCoordinates);

    // Status bit, that will be set to 1 by controller if goal is reached
    int status = 0;

    Speed speed;
    // Choosing gotoController based on chosen architecture
    switch (architecture) {
      case HYBRID:
        speed = trackPath(&current, path, &gotoRulesetAvoidingObstaclesController, TRACK_MINTRACKDISTANCE, &nextPoint, (void*)&ir, &status);
        break;
      case REACTIVE:
        speed = gotoRulesetAvoidingObstaclesController(&current, &goalPosture, (void*)&ir, &status);
        break;
      case SENSE_PLAN_ACT:
        speed = trackPath(&current, path, &gotoRulesetController, TRACK_MINTRACKDISTANCE, &nextPoint, (void*)&ir, &status);
        break;
      case GRADIENT:
        speed = followGradient(&current, searchMap, &gotoRulesetAvoidingObstaclesController, TRACK_MINTRACKDISTANCE, (void*)&ir, &status);
    }
    
    setSpeed(speed);

    // If controller outputs that it's done, end the program
    if (status != 0) {
      break;
    };

    // Output posture for debug purposes
    print_posture(&current);

    // Make LED-ring blink, longer if closer to wall
    showDistanceWithLedRing(ir, (i % 3 + 2) * 200);
    // SAMPLING_TIME msec pause
    Sleep(SAMPLING_TIME);
  }

  // If Loop exited, stop robot
  Stop();

  // Print out final location and end program
  printf("\nFINAL: ");
  print_posture(&current);

  // Save tracked path
  if (trackOutputFile != NULL) {
    path_save(trackOutputFile, trackedPath);
  }

  // Flip map back and then save it in specified file
  if (mapOutputPath != NULL) {
    Map* flippedOutputMap = map_flipHorizontally(obstacleMap);
    SaveMapToFile(flippedOutputMap, SYM, mapOutputPath);
    FreeMap(flippedOutputMap);
  }

  // Clean unused space in memory
  path_free(trackedPath);
  FreeList(list);
  path_free(path);
  FreeMap(obstacleMap);
  FreeMap(searchMap);
}

// argp-structure to store parsed command line arguments
struct arguments {
  enum Architecture architecture;
  enum Algorithm searchAlgorithm;
  PostureDouble start;
  PostureDouble goal;
  char* mapOutputFile;
  char* mapInputFile;
  char* trackOutputFile;
  char* robotNumber;
};

// parses a string to a double, sets toBeSet to obtained value if successful
bool parseDouble(char* string, double* toBeSet) {
  if (string == NULL) return false;
  double result;
  if (sscanf(string, "%lf", &result) == 1) {
    *toBeSet = result;
    return true;
  }

  return false;
}

// Function returning the algorithm enum value of the entered string
bool parseAlgorithm(char* string, enum Algorithm* toBeSet) {
  if (string == NULL)
    return false;
  else if (strcmp(string, "ASTAR") == 0) {
    *toBeSet = ASTAR;
    return true;
  } else if (strcmp(string, "BFS") == 0) {
    *toBeSet = BFS;
    return true;
  } else if (strcmp(string, "DFS") == 0) {
    *toBeSet = DFS;
    return true;
  } else if (strcmp(string, "HEURISTIC") == 0) {
    *toBeSet = HEURISTIC;
    return true;
  } else
    return false;
}

// Function returning the architecture enum value of the given string
bool parseArchitecture(char* string, enum Architecture* toBeSet) {
  if (string == NULL)
    return false;
  else if (strcmp(string, "HYBRID") == 0) {
    *toBeSet = HYBRID;
    return true;
  } else if (strcmp(string, "SENSE_PLAN_ACT") == 0) {
    *toBeSet = SENSE_PLAN_ACT;
    return true;
  } else if (strcmp(string, "REACTIVE") == 0) {
    *toBeSet = REACTIVE;
    return true;
  } else if (strcmp(string, "GRADIENT") == 0) {
    *toBeSet = GRADIENT;
    return true;
  } else
    return false;
}

// Command line argument parser, as specivied in argp-documentation
error_t parse_opt(int key, char* arg, struct argp_state* state) {
  struct arguments* arguments = state->input;

  double parsed;
  switch (key) {
    case 'a':
      if (!parseArchitecture(arg, &(arguments->architecture))) argp_usage(state);
      break;
    case 's':
      if (!parseAlgorithm(arg, &(arguments->searchAlgorithm))) argp_usage(state);
      break;
    case 'x':
      if (parseDouble(arg, &parsed))
        arguments->start.x = parsed;
      else
        argp_usage(state);
      break;
    case 'y':
      if (parseDouble(arg, &parsed))
        arguments->start.y = parsed;
      else
        argp_usage(state);
      break;
    case 'r':
      if (parseDouble(arg, &parsed)) {
        arguments->start.theta = parsed * (2 * PI / 360);
        arguments->start.absoluteTheta = parsed * (2 * PI / 360);
      } else
        argp_usage(state);
      break;
    case 'o':
      arguments->mapOutputFile = arg;
      break;
    case 'i':
      arguments->mapInputFile = arg;
      break;
    case 't':
      arguments->trackOutputFile = arg;
      break;
    case 'n':
      if (arg) arguments->robotNumber = arg;
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num >= 2)
        argp_usage(state);
      else if (state->arg_num == 0) {
        if (parseDouble(arg, &parsed))
          arguments->goal.x = parsed;
        else
          argp_usage(state);
        break;
      } else if (state->arg_num == 1) {
        if (parseDouble(arg, &parsed))
          arguments->goal.y = parsed;
        else
          argp_usage(state);
        break;
      }
      break;
    case ARGP_KEY_END:
      if (state->arg_num != 0 && state->arg_num < 2) /* Not enough arguments. */
        argp_usage(state);
      break;
    default:
      return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

int main(int argc, char** argv) {
  // Initializing documentation and parsing options for argp
  char* argp_program_version = "ePuck GOTO v0.1";
  char* argp_program_bug_address = "Team 3 @ AIMR 2017";
  char doc[] = "A program making the robot go to the specified coordinates on the map";

  char args_doc[] = "x y";

  struct argp_option options[] = {{"architecture", 'a', "ENUM", NULL, "Architecture to be used [HYBRID, SENSE_PLAN_ACT, REACTIVE]"},
                                  {"search-algorithm", 's', "ENUM", NULL, "Search-Algorithm to be used [ASTAR, BFS, DFS, HEURISTIC]"},
                                  {"start-x", 'x', "DOUBLE", NULL, "X-Coordinates of the starting point"},
                                  {"start-y", 'y', "DOUBLE", NULL, "Y-Coordinates of the starting point"},
                                  {"start-theta", 'r', "DOUBLE", NULL, "Angle the robot starts in"},
                                  {"map-output", 'o', "FILE", NULL, "Output file for map"},
                                  {"map-input", 'i', "FILE", NULL, "Input file for map"},
                                  {"track-output", 't', "FILE", NULL, "Input file for map"},
                                  {"robot-number", 'n', "NUMBER", NULL, "Robot number"},
                                  {0}};

  // setting default values
  struct arguments arguments;
  arguments.searchAlgorithm = ASTAR;
  arguments.architecture = HYBRID;
  arguments.mapInputFile = NULL;
  arguments.mapOutputFile = NULL;
  arguments.trackOutputFile = NULL;
  arguments.start.x = 500;
  arguments.start.y = 100;
  arguments.start.theta = 0.5 * PI;
  arguments.start.absoluteTheta = 0.5 * PI;
  arguments.goal.x = 100;
  arguments.goal.y = 500;
  arguments.goal.theta = 0;
  arguments.goal.absoluteTheta = 0;
  arguments.robotNumber = "3";

  // parsing command line arguments
  struct argp argp = {options, parse_opt, args_doc, doc};
  argp_parse(&argp, argc, argv, 0, 0, &arguments);

  // starting main control loop
  runControlLoop(arguments.robotNumber, arguments.start, arguments.goal, arguments.searchAlgorithm, arguments.architecture,
                 arguments.mapInputFile, arguments.mapOutputFile, arguments.trackOutputFile);

  return 0;
}
