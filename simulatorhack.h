#ifndef SIMULATORHACK_H
#define SIMULATORHACK_H

// This header file was written to replace some definitions possible using the simulator
// to make exactly the same code work with both compilation settings

#ifdef USING_SIMULATOR
typedef int bool;
#define true 1
#define false 0
#define RING_LEDS_NUM 8
#define SetRingLED(param) ;
#define max(x, y) (x > y ? x : y)
#define min(x, y) (x > y ? y : x)
#endif

#endif